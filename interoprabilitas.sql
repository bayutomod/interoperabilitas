/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 10.4.21-MariaDB : Database - interop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`interop` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `interop`;

/*Table structure for table `etd` */

DROP TABLE IF EXISTS `etd`;

CREATE TABLE `etd` (
  `etd_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `etd_intisari` text DEFAULT NULL,
  `etd_abstract` text DEFAULT NULL,
  `etd_pembimbing` varchar(150) DEFAULT NULL,
  `etd_keyword` varchar(250) DEFAULT NULL,
  `etd_jenis` enum('Tesis','Skripsi','Tugas Akhir') DEFAULT NULL,
  `etd_tahun` year(4) DEFAULT NULL,
  `etd_is_publikasi` tinyint(4) DEFAULT NULL,
  `etd_metadata_stat` tinyint(4) DEFAULT NULL COMMENT 'metadata',
  `etd_metadata_komentar` varchar(250) DEFAULT NULL COMMENT 'metadata komentar',
  `etd_ijinpublikasi_stat` tinyint(4) DEFAULT NULL COMMENT 'ijinpublikasi jika di publish',
  `etd_ijinpublikasi_komentar` varchar(250) DEFAULT NULL COMMENT 'ijinpublikasi jika di publish',
  PRIMARY KEY (`etd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `etd` */

/*Table structure for table `etd_file` */

DROP TABLE IF EXISTS `etd_file`;

CREATE TABLE `etd_file` (
  `etd_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `etd_file_filename` varchar(150) DEFAULT NULL,
  `etd_file_stat` tinyint(4) DEFAULT NULL COMMENT '1:disetujui,2:ditolak',
  `etd_file_komentar` varchar(250) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`etd_id`,`file_id`),
  KEY `file_id` (`file_id`),
  CONSTRAINT `etd_file_ibfk_1` FOREIGN KEY (`etd_id`) REFERENCES `etd` (`etd_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `etd_file_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`file_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `etd_file` */

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `file_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `file` */

insert  into `file`(`file_id`,`file_name`) values 
(1,'File Fulltext (.pdf)'),
(2,'File Summary (.pdf)'),
(3,'File Naskah Publikasi (.pdf)'),
(4,'File Halaman Judul & Pengesahan (.pdf)'),
(5,'File Abstrak/Intisari & Abstract (.pdf)'),
(6,'File Daftar Isi (.pdf)'),
(7,'File Pendahuluan (.pdf)'),
(8,'File Penutup (.pdf)'),
(9,'File Daftar Pustaka (.pdf)');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
